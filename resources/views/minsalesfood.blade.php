@extends('layouts.main')
@section('content')
@section('title', 'Админ панель')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Min Sales Food</h1>
        </div>
    </div>
</div>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Create Category
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="create_min_sales_food_form" action="{{ url('admin/createMinSalesFood') }}" method="post">
                    {{ csrf_field() }}
                    <label for="recipient-name" class="col-form-label">
                        <strong>
                                    <span class="span">
                                       img:
                                    </span>
                        </strong>
                    </label>
                    <input type="text" name="img" class="form-control" >

                    <strong>
                                    <span class="span">
                                       title:
                                    </span>
                    </strong>
                    </label>
                    <input type="text" name="title" class="form-control" >
                    <strong>
                                    <span class="span">
                                       name:
                                    </span>
                    </strong>
                    </label>
                    <input type="text" name="name" class="form-control" >
                    <strong>
                                    <span class="span">
                                       price:
                                    </span>
                    </strong>
                    </label>
                    <input type="text" name="price" class="form-control" >
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary btn_add" value="Save" >
                    </div>
            </div>
        </div>
    </div>
    </form>

</div>



<table class="table Bordered Table responsive">
    <thead>
    <tr>
        <th>ID</th>
        <th>img</th>
        <th>title</th>
        <th>name</th>
        <th>price</th>
        <th class="edit-del"></th>
        <th class="edit-del"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($minSalesFoods as $minSalesFood)
        <tr>
            <form class="edit_Min_sales_Food" action="{{ url('admin/editMinSalesFood') }}" method="post">
                {{ csrf_field() }}
                <td>
                    {{$minSalesFood->id}}
                </td>
                <td>
                        <span>

                            {{$minSalesFood->img}}
                        </span>
                    <input type="text" name="img" class="form-control" value="{{$minSalesFood->img}}" style="display: none;">
                </td>

                <td>
                        <span>

                            {{$minSalesFood->title}}
                        </span>
                    <input type="text" name="title" class="form-control" value="{{$minSalesFood->title}}" style="display: none;">
                </td>
                <td>
                        <span>

                            {{$minSalesFood->name}}
                        </span>
                    <input type="text" name="name" class="form-control" value="{{$minSalesFood->name}}" style="display: none;">
                </td>
                <td>
                        <span>

                            {{$minSalesFood->price}}
                        </span>
                    <input type="text" name="price" class="form-control" value="{{$minSalesFood->price}}" style="display: none;">
                </td>
                <td>
                    <a class="edit_texts_Min_sales_Food fa fa-pencil " type="button" class="btn btn-primary" >
                    </a>
                    <button type="submit" class="btn btn-info btn-circle " style="display: none;">
                        <i class="fa fa fa-pencil"></i>
                    </button>
                    <input type="hidden" name="id" class="form-control" value="{{$minSalesFood->id}}">
                </td>
                <td>
                    <a class="fa fa-trash this-trash-Min_Sales-food" href="{{ route('admin.deleteMinSalesFood',["id"=>$minSalesFood->id]) }}">
                    </a>
                </td>
            </form>
        </tr>
    @endforeach
    </tbody>
</table>

<script>


    $(document).on('click', '.edit_texts_Min_sales_Food', function () {
        $(this).closest('tr').find('td span').hide();

        $(this).closest('tr').find('td input').show();

        $(this).hide().next().show();
    });
    $(document).on('click', '.this-trash-Min_Sales-food', function(event){
        event.preventDefault();

         toastr.success('Судейство удалилось' + '');
         toastr.options.closeDuration = 2000;

        var $this = $(this);

        $.get($this.attr('href'), function(){
            $this.closest('tr').fadeOut();
        });
    });

    $(document).on('submit', '.edit_Min_sales_Food', function(event){
        event.preventDefault();

        $this = $(this);

        $.ajax({
            type: $this.attr('method'),
            url: $this.attr('action'),
            data: $this.serialize(),

            success: function(response){
                if(response.status === "ok"){
                    toastr.success(response.message);
                }
                else{
                    toastr.error('Something was wrong, pleas try again');
                }
                toastr.options.closeDuration = 2000;
                $this.closest('tr').find('td input').each(function(){
                    $(this).siblings('span').text($(this).val());
                });

                $this.closest('tr').find('td span').show();
                $this.closest('tr').find('td input').hide();

                $('.edit_texts_Min_sales_Food').show().next().hide();

            }
        });
    });

    $(document).on('submit', '.create_min_sales_food_form', function (event) {
        event.preventDefault();

        $this = $(this);

        $.ajax({
            type: $this.attr('method'),
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (res) {
                $('#exampleModal').modal('toggle');
                $this[0].reset();
                if (res.status === "ok") {
                    toastr.success(res.message);
                    var row = $(".table tbody tr:first").clone();
                    row[0].cells[0].innerHTML = res.data.id;
                    row[0].cells[1].innerHTML = res.data.img;
                    row[0].cells[2].innerHTML = res.data.title;
                    row[0].cells[3].innerHTML = res.data.name;
                    row[0].cells[4].innerHTML = res.data.price;

                    $(row[0].cells[5]).find("input[name=id]:first").val(res.data.id);
                    var href = $(row[0].cells[6]).find("a")[0].href.replace(/\d+/g, res.data.id);
                    $(row[0].cells[6]).find("a")[0].href = href;
                    $(".table tbody").append(row);
                }
                else {
                    toastr.error('Something was wrong, pleas try again');
                }
                toastr.options.closeDuration = 2000;
            }
        });
    });

</script>
<script src="/js/ajax.js"></script>
<script src="/js/toastr.js"></script>
@endsection