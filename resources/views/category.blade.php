@extends('layouts.main')
@section('content')
@section('title', 'Админ панель')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Category</h1>
        </div>
    </div>
</div>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Create Category
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="create_Category_form" action="{{ url('admin/createCategory') }}" method="post">
                    {{ csrf_field() }}
                    <label for="recipient-name" class="col-form-label">
                        <strong>
                                    <span class="span">
                                       Title:
                                    </span>
                        </strong>
                    </label>
                    <input type="text" name="title" class="form-control" >

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn_add" value="Save" >
            </div>
        </div>
    </div>
</div>
</form>

            </div>



<table class="table Bordered Table responsive">
    <thead>
    <tr>
        <th>ID</th>
        <th>title</th>
        <th class="edit-del"></th>
        <th class="edit-del"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($Categorys as $category)
        <tr>
            <form class="edit_Category" action="{{ url('admin/editCategory') }}" method="post">
                {{ csrf_field() }}
                <td>
                    {{$category->id}}
                </td>
                <td>
                        <span>

                            {{$category->title}}
                        </span>
                    <input type="text" name="title" class="form-control" value="{{$category->title}}" style="display: none;">
                </td>

                <td>
                    <a class="edit_texts fa fa-pencil " type="button" class="btn btn-primary" >
                    </a>
                    <button type="submit" class="btn btn-info btn-circle " style="display: none;">
                        <i class="fa fa fa-pencil"></i>
                    </button>
                    <input type="hidden" name="id" class="form-control" value="{{$category->id}}">
                </td>
                <td>
                    <a class="fa fa-trash  this-trashs" href="{{ route('admin.deleteCategory',["id"=>$category->id]) }}">
                    </a>
                </td>
            </form>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).on('submit', '.create_Category_form', function (event) {
        event.preventDefault();

        $this = $(this);

        $.ajax({
            type: $this.attr('method'),
            url: $this.attr('action'),
            data: $this.serialize(),
            success: function (res) {
                $('#exampleModal').modal('toggle');
                $this[0].reset();
                if (res.status === "ok") {
                    toastr.success(response.message);
                    var row = $(".table tbody tr:first").clone();
                    row[0].cells[0].innerHTML = res.data.id;
                    row[0].cells[1].innerHTML = res.data.title;

                    $(row[0].cells[2]).find("input[name=id]:first").val(res.data.id);
                    var href = $(row[0].cells[3]).find("a")[0].href.replace(/\d+/g, res.data.id);
                    $(row[0].cells[3]).find("a")[0].href = href;
                    $(".table tbody").append(row);
                }
                else {

                }

            }
        });
    });

</script>
<script src="/js/toastr.js"></script>
<script src="/js/ajax.js"></script>
@endsection