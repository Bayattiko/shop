@extends('layouts.main')
@section('content')
@section('title', 'Админ панель')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Users</h1>
        </div>
    </div>
</div>


<table class="table Bordered Table responsive">
    <thead>
    <tr>
        <th>ID</th>
        <th>name</th>
        <th>email</th>
        <th>password</th>
        <th class="edit-del"></th>
        <th class="edit-del"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <form class="edit_User" action="{{ url('admin/editUser') }}" method="post">
                {{ csrf_field() }}
                <td>
                    {{$user->id}}
                </td>
                <td>
                        <span>

                            {{$user->name}}
                        </span>
                    <input type="text" name="name" class="form-control" value="{{$user->name}}" style="display: none;">
                </td>
                <td>
                        <span>

                            {{$user->email}}
                        </span>
                    <input type="text" name="email" class="form-control" value="{{$user->email}}" style="display: none;">
                </td>
                <td>
                        <span>

                            {{$user->password}}
                        </span>
                    <input type="text" name="password" class="form-control" value="{{$user->password}}" style="display: none;">
                </td>

                <td>
                    <a class="edit_User_texts fa fa-pencil " type="button" class="btn btn-primary" >
                    </a>
                    <button type="submit" class="btn btn-info btn-circle " style="display: none;">
                        <i class="fa fa fa-pencil"></i>
                    </button>
                    <input type="hidden" name="id" class="form-control" value="{{$user->id}}">
                </td>
                <td>
                    <a class="fa fa-trash  this-trash_User" href="{{ route('admin.deleteUser',["id"=>$user->id]) }}">
                    </a>
                </td>
            </form>
        </tr>
    @endforeach
    </tbody>
</table>

<script src="/js/ajax.js"></script>
<script src="/js/toastr.js"></script>
@endsection