<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href=" /css/docs.css" rel="stylesheet">
    <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet"><!--+-->
    <link href="/css/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href=" /css/sb-admin-2.css" rel="stylesheet">
    <link href=" /css/tables.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/docs.css">
    <link href="/css/Merris/morris.css" rel="stylesheet">
    <link href="{{ asset('/css/toastr.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="all" href="{{ asset('css/site.css') }}">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<!-- /#wrapper -->
<div id="wrapper">
    @section('sidebar')
        @include("elements.sidebar")
    @show
    <div id="page-wrapper" class="container-fluid">
        @yield('content')
    </div>
</div>
<!-- /#wrapper -->
<script src="/vendor/ckeditor/ckeditor.js"></script>

<!-- jQuery -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ asset('/vendor/metisMenu/metisMenu.min.js') }}"></script>

<!-- Morris Charts JavaScript -->
<!--<script src="../vendor/raphael/raphael.min.js"></script>
<script src="../vendor/morrisjs/morris.min.js"></script>
<script src="../data/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="{{ asset('/js/sb-admin-2.js') }}"></script>

<script src="/js/custom.js" rel="stylesheet"></script>
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/js/ajax.js"></script>
</body>
</html>