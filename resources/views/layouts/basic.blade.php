<?php
$language = [
    'en'=>'ENG',
    'ru'=>'РУС'
]
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Международная федерация рукопашного боя</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="theme-color" content="#fff">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Page description">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--Twitter Card data-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <!--Open Graph data-->
    <meta property="og:title" content="Title Here">
    <meta property="og:type" content="article">
    <meta property="og:url" content="http://www.example.com/">
    <meta property="og:image" content="http://example.com/image.jpg">
    <meta property="og:description" content="Description Here">
    <meta property="og:site_name" content="Site Name, i.e. Moz">
    <meta property="fb:admins" content="Facebook numeric ID">
    <link rel="stylesheet" media="all" href="{{ asset('site/css/app.css') }}">
    <link rel="stylesheet" media="all" href="{{ asset('css/site.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
<div class="out" id="app">
    <div class="layout">
        <div class="sidebar js-sidebar">
            <div class="sidebar__head"><button class="sidebar__burger js-sidebar-burger"><span></span></button>
                <a class="sidebar__org org org_head" href="{{  route('main') }}">
                    <div class="org__title">HSIF</div>
                    <div class="org__text">МЕЖДУНАРОДНАЯ ФЕДЕРАЦИЯ РУКОПАШНОГО БОЯ</div>
                </a>
            </div>
            <div class="sidebar__container js-sidebar-container">
                <div class="sidebar__wrap"><a class="sidebar__logo logo" href="{{  route('main') }}"></a>
                    @section('sidebarGroup')
                    @show

                    @section('sidebar')
                    @show
                </div>
                <div class="sidebar__foot">
                    <div class="sidebar__section">
                        <div class="org">
                            <div class="org__title">HSIF</div>
                            <div class="org__text">{{ trans('about.INTERNATIONAL FEDERATION OF FIGHT AGENCY') }}</div>
                            <a class="org__phone" href="tel:+74991405172">+7 499 140-51-72</a><a class="org__email" href="mailto:info@hsif-international.com">info@hsif-international.com</a>
                        </div>
                    </div>
                    <div class="sidebar__section">
                        <div class="socials">
                            <div class="socials__list">
                                <a class="socials__link" href="#">
                                    <svg class="icon icon-instagram">
                                        <use xlink:href="{{ asset('site/img/sprite.svg#icon-instagram') }}"></use>
                                    </svg>
                                </a>
                                <a class="socials__link" href="#">
                                    <svg class="icon icon-facebook">
                                        <use xlink:href="{{ asset('site/img/sprite.svg#icon-facebook') }}"></use>
                                    </svg>
                                </a>
                            </div>
                            @if ( strtolower(app()->getLocale()) === "ru")
                                <a id="languageSwitcher" class="socials__lang" href="javascript:void(0)" data-value="en">{!! $language['en'] !!}</a>
                            @else
                                <button id="languageSwitcher" class="socials__lang" data-value="ru">{!! $language['ru'] !!}</button>
                            @endif
                        </div>
                    </div>
                    <div class="sidebar__section">
                        <div class="copyright">
                            <div class="copyright__title">2018, {{ trans('about.All rights reserved') }}</div>
                            <div class="copyright__text">{{ trans('about.Use of the materials') }}</div>
                        </div>
                    </div>
                    <div class="sidebar__section">
                        <a class="created" href="https://smd.agency/" target="_blank">{{ trans('about.sitecreated') }} <img class="created__smd" src='{{ asset('site/img/smd.svg') }}' alt=''></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="center">
            @yield('content')
        </div>
    </div>
</div>
<div class="lightbox js-lightbox">
    <div class="lightbox__wrap js-lightbox-wrap">
        <div class="lightbox__sidebar">
            <button class="lightbox__close js-lightbox-close"><span class="lightbox__label">Закрыть</span>
                <span class="lightbox__icon">
                    <svg class="icon icon-close"><use xlink:href="{{ asset('site/img/sprite.svg#icon-close') }}"></use>
                    </svg>
                </span>
            </button>
            <div class="lightbox__content">
                <div class="lightbox__title js-lightbox-title">Title</div>
                <div class="lightbox__text js-lightbox-text">Text</div>
            </div>
            <div class="lightbox__location location">
                <div class="location__icon"><svg class="icon icon-point"><use xlink:href="{{ asset('site/img/sprite.svg#icon-point') }}"></use></svg></div>
                <div class="location__title js-lightbox-location">Location</div>
            </div>
        </div>
        <div class="lightbox__container">
            <button class="lightbox__prev js-lightbox-prev">
                <svg class="icon icon-arrow-left">
                    <use xlink:href="{{ asset('site/img/sprite.svg#icon-arrow-left') }}"></use>
                </svg>
            </button><button class="lightbox__next js-lightbox-next">
                <svg class="icon icon-arrow-right">
                    <use xlink:href="{{ asset('site/img/sprite.svg#icon-arrow-right') }}"></use>
                </svg>
            </button>
            <img class="lightbox__pic js-lightbox-pic" src="{{ asset('site/img/pic3.jpg') }}"></div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('site/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/ajax.js') }}"></script>
</body>

</html>

