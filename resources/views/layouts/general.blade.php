<?php

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="theme-color" content="#fff">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Page description">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Twitter Card data-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="http://www.example.com/image.jpg">

    <!--Open Graph data-->
    <meta property="og:title" content="Title Here">
    <meta property="og:type" content="article">
    <meta property="og:url" content="http://www.example.com/">
    <meta property="og:image" content="http://example.com/image.jpg">
    <meta property="og:description" content="Description Here">
    <meta property="og:site_name" content="Site Name, i.e. Moz">
    <meta property="fb:admins" content="Facebook numeric ID">
    <link rel="stylesheet" media="all" href="{{ asset('/site/css/app.css') }}">
    <link rel="stylesheet" media="all" href="{{ asset('css/site.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
<div class="out" id="app">
    <div class="start">
        <header class="start__header">
            <div class="start__center center">
                <div class="start__container"><a class="start__logo logo" href="{{ route('main') }}"></a>
                    <div class="start__title">{{ trans('main.INTERNATIONAL FEDERATION OF FIGHT AGENCY') }}</div>
                    @section('tools')
                    @show
                </div>
            </div>
        </header>

        @yield('content');

        @section('footer')
            @include("elements.gFooter")
        @show
    </div>
</div>
<div class="lightbox js-lightbox" >
    <div class="lightbox__wrap js-lightbox-wrap">
        <div class="lightbox__sidebar"><button class="lightbox__close js-lightbox-close">
                <span class="lightbox__label">Закрыть</span>
                <span class="lightbox__icon"><svg class="icon icon-close"><use xlink:href="{{ asset('site/img/sprite.svg#icon-close') }}"></use></svg></span></button>
            <div class="lightbox__content">
                <div class="lightbox__title js-lightbox-title">Title</div>
                <div class="lightbox__text js-lightbox-text">Text</div>
            </div>
            <div class="lightbox__location location">
                <div class="location__icon"><svg class="icon icon-point"><use xlink:href="{{ asset('site/img/sprite.svg#icon-point') }}"></use></svg></div>
                <div class="location__title js-lightbox-location">Location</div>
            </div>
        </div>
        <div class="lightbox__container">
            <button class="lightbox__prev js-lightbox-prev"><svg class="icon icon-arrow-left"><use xlink:href="{{ asset('site/img/sprite.svg#icon-arrow-left')}}"></use></svg></button>
            <button class="lightbox__next js-lightbox-next"><svg class="icon icon-arrow-right"><use xlink:href="{{ asset('site/img/sprite.svg#icon-arrow-right') }}"></use></svg></button>
            <img class="lightbox__pic js-lightbox-pic" src="{{ asset('site/img/pic3.jpg') }}">
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('/js/app.js') }}"></script>
<script type="text/javascript" src="{{asset('/site/js/app.js') }}"></script>
<script src="{{ asset('/js/custom.js') }}" rel="stylesheet"></script>
<script>
    function initMap()
    {
        if(document.getElementById('map')){
            var uluru = {
                lat: 55.928119,
                lng: 37.762673
            };
            var map = new google.maps.Map(
                document.getElementById('map'),
                {
                    zoom: 14,
                    center: uluru
                });
            var marker = new google.maps.Marker(
                {
                    position: uluru,
                    map: map
                });
        }
    }

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
</body>

</html>

