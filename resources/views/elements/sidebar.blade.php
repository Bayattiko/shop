
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>

                </button>
                <a class="navbar-brand" href="/" style="padding: 0 0;">

                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">

                    <ul class="dropdown-menu dropdown-tasks">

                        <li class="divider"></li>

                        <li class="divider"></li>

                        <li class="divider"></li>

                        <li class="divider"></li>

                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">

                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>

                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>


                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li> <a href="<?php echo e(route('logout')); ?>"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <p style="padding-top:7px;">Выход</p>
                            </a>
                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form>
                        </li>
                       {{-- <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>--}}
                        </li>
                       {{-- <li class="divider">

                        </li>--}}
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
                <li class="lng-block">
                    <div class="btn-group" role="group" id="languageSwitcher" >

                    </div>
                </li>
                <!-- /.lng-block -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">



                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Foods<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('admin.food') }}"><i class="fa fa-table fa-fw"></i> Foods</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.topFood') }}">Top Foods</a>
                                </li>
                                 <li>
                                     <a href="{{ route('admin.MinSalesFood') }}">Min Sales food</a>
                                 </li>
                                <li>
                                     <a href="{{ route('admin.endingFood') }}">Ending Foods</a>
                                 </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                      {{--  <li>
                            <a href="{{ route('admin.food') }}"><i class="fa fa-table fa-fw"></i> Foods</a>
                        </li>--}}
                        <li>
                            <a href="{{ route('admin.users') }}"><i class="fa fa-dashboard fa-fw"></i> Users</a>
                        </li>
                        <li>
                       {{--     <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> ....<span class="fa arrow"></span></a>--}}
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/test">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('admin') }}"><i class="fa fa-table fa-fw"></i> Category</a>
                        </li>

                  {{--      <li>
                           <a href="#"><i class="fa fa-wrench fa-fw"></i> Category<span class="fa arrow"></span></a>


                            <ul class="nav nav-second-level">

                              --}}{{--  <li>
                                    <a href="{{ route('admin.pdffiles') }}"><i class="fa fa-file-text"> </i> Создать</a>
                                </li>--}}{{--
                             --}}{{--   @foreach($sidebarDocs as  $doc)
                                  --}}{{----}}{{--  <li>
                                        <a href="{{ route('admin.doc-files',['alias'=>$doc->alias]) }}"><i class="fa fa-file-text"></i> {{$doc->title}}</a>
                                    </li>--}}{{----}}{{--
                                @endforeach--}}{{--
                                <li>
                                --}}{{--    <a href="grid.html">Grid</a>--}}{{--
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>--}}
                        <li>
                           {{-- <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>--}}
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>