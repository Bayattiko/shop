<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\ShopUser;
use App\Food;
use App\TopFood;
use App\SaleFood;
use App\EndingFood;

class AdminController extends Controller
{

    public function getCategories() {
        $Categorys = Category::get();
        return view ("category")
        ->with('Categorys',$Categorys);
    }

    public function deleteCategory(Request $request) {

        $categorys = Category::find($request->id);
        if($categorys && $categorys->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function editCategory(Request $request)
    {

        $Category = Category::find($request->input('id'));
        if($Category) {
            $Category->title = $request->input('title');

            if ($Category->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

    public function getUsers() {

        $users = ShopUser::get();
        return view ("users")
            ->with('users',$users);

    }

    public function deleteUser(Request $request) {

        $users = ShopUser::find($request->id);
        if($users && $users->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function editUser(Request $request)
    {

        $users = ShopUser::find($request->input('id'));
        if($users) {
            $users->name = $request->input('name');
            $users->email = $request->input('email');
            $users->password = $request->input('password');
            if ($users->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

    public function createCategory(Request $request) {

        $category = new Category;
        $category->title = $request->input('title');


        if($category && $category->save()) {
            return response()->json(["status"=>"ok","message"=>"Сохранено","data"=>$category],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function createTopFood(Request $request) {

        $topfood = new TopFood;
        $topfood->img = $request->input('img');
        $topfood->title = $request->input('title');
        $topfood->name = $request->input('name');
        $topfood->price = $request->input('price');

        if($topfood && $topfood->save()) {
            return response()->json(["status"=>"ok","message"=>"Сохранено","data"=>$topfood],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function createMinSalesFood(Request $request) {

        $minSalesFood = new SaleFood;
        $minSalesFood->img = $request->input('img');
        $minSalesFood->title = $request->input('title');
        $minSalesFood->name = $request->input('name');
        $minSalesFood->price = $request->input('price');

        if($minSalesFood && $minSalesFood->save()) {
            return response()->json(["status"=>"ok","message"=>"Сохранено","data"=>$minSalesFood],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function createEndingFood(Request $request) {

        $endingFood = new EndingFood;
        $endingFood->img = $request->input('img');
        $endingFood->title = $request->input('title');
        $endingFood->name = $request->input('name');
        $endingFood->price = $request->input('price');

        if($endingFood && $endingFood->save()) {
            return response()->json(["status"=>"ok","message"=>"Сохранено","data"=>$endingFood],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }
    public function getFoods() {

        $foods = Food::get();
        return view ("food")
            ->with('foods',$foods);

    }

    public function getTopFoods() {

        $topFoods = TopFood::get();
        return view ("topFoods")
            ->with('topFoods',$topFoods);

    }

    public function getEndingFood() {
        $endingFoods = EndingFood::get();
        return view ("endingfood")
            ->with('endingFoods',$endingFoods);
    }

    public function getMinSalesFood() {

        $minSalesFoods = SaleFood::get();
        return view ("minsalesfood")
            ->with('minSalesFoods',$minSalesFoods);

    }

    public function createFood(Request $request) {

        $food = new Food;
        $food->img = $request->input('img');
        $food->title = $request->input('title');
        $food->name = $request->input('name');
        $food->price = $request->input('price');

        if($food && $food->save()) {
            return response()->json(["status"=>"ok","message"=>"Сохранено","data"=>$food],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }
    public function deleteFood(Request $request) {

        $foods = Food::find($request->id);
        if($foods && $foods->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function deleteTopFood(Request $request) {

        $TopFood = TopFood::find($request->id);
        if($TopFood && $TopFood->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }
    public function deleteMinSalesFood(Request $request) {

        $foods = SaleFood::find($request->id);
        if($foods && $foods->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function deleteEndingFood(Request $request) {


        $endingFood = EndingFood::find($request->id);
        if($endingFood && $endingFood->delete()) {
            return response()->json(["status"=>"ok","message"=>"Удалено"],200);
        }else{
            return response()->json(["status"=>"error","message"=>"Не найдено"],400);
        }
    }

    public function editFood(Request $request)
    {

        $foods = Food::find($request->input('id'));
        if($foods) {
            $foods->img = $request->input('img');
            $foods->title = $request->input('title');
            $foods->name = $request->input('name');
            $foods->price = $request->input('price');
            if ($foods->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

    public function editTopFood(Request $request)
    {

        $topFood = TopFood::find($request->input('id'));
        if($topFood) {
            $topFood->img = $request->input('img');
            $topFood->title = $request->input('title');
            $topFood->name = $request->input('name');
            $topFood->price = $request->input('price');
            if ($topFood->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

    public function editMinSalesFood(Request $request)
    {

        $topFood = SaleFood::find($request->input('id'));
        if($topFood) {
            $topFood->img = $request->input('img');
            $topFood->title = $request->input('title');
            $topFood->name = $request->input('name');
            $topFood->price = $request->input('price');
            if ($topFood->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

    public function editEndingFood(Request $request)
    {

        $endingFood = EndingFood::find($request->input('id'));
        if($endingFood) {
            $endingFood->img = $request->input('img');
            $endingFood->title = $request->input('title');
            $endingFood->name = $request->input('name');
            $endingFood->price = $request->input('price');
            if ($endingFood->save()) {

                return response()->json(["status" => "ok", "message" => "Изменено"], 200);
            } else {
                return response()->json(["status" => "error", "message" => "Не найдено"], 400);
            }
        }
        else{
            return response()->json(["status" => "ok", "message" => "Изменено"], 400);
        }

    }

}
