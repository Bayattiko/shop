<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopUser extends Model
{
    protected $table ='_shop_users';
    public    $timestamps = false;
    protected $fillable = ['name','email','password'];

}
