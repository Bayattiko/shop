<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndingFood extends Model
{
    protected $table ='_ending_foods';
    public $timestamps = false;
    protected $fillable = ['img','title','name','price'];
}
