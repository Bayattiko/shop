<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopFood extends Model
{
    protected $table ='_top_foods';
    public $timestamps = false;
    protected $fillable = ['img','title','name','price'];
}
