<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table ='_categories';
    public $timestamps = false;
    protected $fillable = ['title'];
}
