<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleFood extends Model
{
    protected $table ='min_salefoods';
    public $timestamps = false;
    protected $fillable = ['img','title','name','price'];
}
