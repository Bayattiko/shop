<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


    Route::group(['prefix'=>'admin','middleware' => ['auth']], function() {


    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'AdminController@getCategories')->name('admin');
    Route::get('/users', 'AdminController@getUsers')->name('admin.users');
    Route::get('/food', 'AdminController@getFoods')->name('admin.food');
    Route::get('/topFood', 'AdminController@getTopFoods')->name('admin.topFood');
    Route::get('/minSalesFood', 'AdminController@getMinSalesFood')->name('admin.MinSalesFood');
    Route::get('/endingFood', 'AdminController@getEndingFood')->name('admin.endingFood');
    Route::get('/deleteCategory/{id}',['uses' => 'AdminController@deleteCategory'])->name('admin.deleteCategory');
    Route::post('editCategory',['uses' => 'AdminController@editCategory'])->name('admin.editCategory');
    Route::get('/deleteUser/{id}',['uses' => 'AdminController@deleteUser'])->name('admin.deleteUser');
    Route::get('/deleteFood/{id}',['uses' => 'AdminController@deleteFood'])->name('admin.deleteFood');
    Route::get('/deleteTopFood/{id}',['uses' => 'AdminController@deleteTopFood'])->name('admin.deleteTopFood');
    Route::get('/deleteMinSalesFood/{id}',['uses' => 'AdminController@deleteMinSalesFood'])->name('admin.deleteMinSalesFood');
    Route::get('/deleteEndingFood/{id}',['uses' => 'AdminController@deleteEndingFood'])->name('admin.deleteEndingFood');
    Route::post('editUser',['uses' => 'AdminController@editUser'])->name('admin.editUser');
    Route::post('editFood',['uses' => 'AdminController@editFood'])->name('admin.editFood');
    Route::post('editTopFood',['uses' => 'AdminController@editTopFood'])->name('admin.editTopFood');
    Route::post('editMinSalesFood',['uses' => 'AdminController@editMinSalesFood'])->name('admin.editMinSalesFood');
    Route::post('editEndingFood',['uses' => 'AdminController@editEndingFood'])->name('admin.editEndingFood');
    Route::post('createCategory',['uses' => 'AdminController@createCategory'])->name('admin.createCategory');
    Route::post('createFood',['uses' => 'AdminController@createFood'])->name('admin.createFood');
    Route::post('createTopFood',['uses' => 'AdminController@createTopFood'])->name('admin.createTopFood');
    Route::post('createMinSalesFood',['uses' => 'AdminController@createMinSalesFood'])->name('admin.createMinSalesFood');
    Route::post('createEndingFood',['uses' => 'AdminController@createEndingFood'])->name('admin.createEndingFood');

});




